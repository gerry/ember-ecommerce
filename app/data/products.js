export const products = [
  {
    id: '1',
    name: 'Beats Solo Wireless Headphones',
    price: {
      original: 199.95,
      current: 99.98,
    },
    features: [
      'High-performance wireless noise cancelling headphones in red',
      'Active Noise Cancelling (ANC) blocks external noise',
      'Transparency helps you stay aware of your surroundings while listening',
      'Features thee Apple H1 Headphone Chip and Class 1 Bluetooth',
      'Compatible with iOS and Android',
      'Up to 22 hours of listening time',
    ],
    colors: [
      {
        color: 'red',
        image: '/assets/images/beats-solo-red.png',
      },
      {
        color: 'black',
        image: '/assets/images/beats-solo-black.png',
      },
    ],
  },
  {
    id: '2',
    name: 'Nike Air Force 1',
    price: {
      original: 109.95,
      current: 89.98,
    },
    features: [
      'Full-grain leather in the upper adds a premium look and feel',
      'Originally designed for performance hoops, Nike Air cushioning adds lightweight, all-day comfort',
      'The padded, low-cut collar looks sleek and feeels great',
    ],
    colors: [
      {
        color: 'white',
        image: '/assets/images/nike-air-force-1-white.png',
      },
    ],
  },
];
